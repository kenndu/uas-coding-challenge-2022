#Kenn Du 19513720 UBC UAS Coding challenge submission 2022
import math

class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y   

    def distanceTo(self, point):
        return math.sqrt((self.x-point.x)**2 + (self.y-point.y)**2)

    def shortestLineSegment(self, lineSeg):
        p1 = Point(self.x,self.y)
        p2 = Point(self.x,self.y)

        p2.x += lineSeg.perpVector[0]
        p2.y += lineSeg.perpVector[1]
        shortestLineSegment = LineSegment(p1,p2)

        normalLine = LineSegment(p1,p2)
        if (lineSeg.intersects(normalLine)):
            shortestLineSegment.endPoint = Point(lineSeg.intersects(normalLine)[0], lineSeg.intersects(normalLine)[1])

        elif (self.distanceTo(lineSeg.startPoint) > self.distanceTo(lineSeg.endPoint)):
            shortestLineSegment.endPoint = lineSeg.endPoint

        else:
            shortestLineSegment.endPoint = lineSeg.startPoint
        
        return shortestLineSegment

    def print(self):
        print(self.x, self.y)


class LineSegment:
    def __init__(self, startPoint, endPoint):
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.dirVector = (self.endPoint.x-self.startPoint.x, self.endPoint.y-self.startPoint.y)
        self.perpVector = (-self.dirVector[1], self.dirVector[0])

    def findStandardCoeffs(self):
        A = self.startPoint.y - self.endPoint.y
        B = self.endPoint.x - self.startPoint.x
        C = self.startPoint.x*self.endPoint.y - self.startPoint.y*self.endPoint.x

        return A, B, -C

    def containsPoint(self, point):

        onLine = math.isclose(point.distanceTo(self.endPoint)+point.distanceTo(self.startPoint), self.startPoint.distanceTo(self.endPoint), rel_tol=1e-5)

        withinXRange = self.startPoint.x <= point.x and self.endPoint.x >= point.x
        withinYRange = (self.startPoint.y <= point.y and self.endPoint.y >= point.y) or (self.startPoint.y >= point.y and self.endPoint.y <= point.y)

        return onLine and withinXRange and withinYRange

    def intersects(self, lineSeg2):
        L1 = self.findStandardCoeffs()
        L2 = lineSeg2.findStandardCoeffs()
        # print("L1:", L1)
        # print("L2:", L2)
        D  = L1[0] * L2[1] - L1[1] * L2[0]
        Dx = L1[2] * L2[1] - L1[1] * L2[2]
        Dy = L1[0] * L2[2] - L1[2] * L2[0]
        # print(D,Dx,Dy)
        if D != 0:
            x = Dx / D
            y = Dy / D
            if self.containsPoint(Point(x,y)):
                # print(x,y)
                return x,y

        return False

    def length(self):
        return math.sqrt((self.endPoint.x-self.startPoint.x)**2+(self.endPoint.y-self.startPoint.y)**2)

    def print(self):
        print(self.startPoint.x, self.startPoint.y)
        print(self.endPoint.x, self.endPoint.y)

    def intersectsCircle(self, circle):
        shortestLineSegment = circle.originPoint.shortestLineSegment(self)
        if (shortestLineSegment.length() < circle.radius):
            print("Intersects twice")
            self.findAltPath(circle)
            return 2
        elif (shortestLineSegment.length() == circle.radius):
            print("Intersects once")
            self.findAltPath(circle)
            return 1
        else:
            print("No intersects")
            return 0

    def findAltPath(self, circle):
        piece1 = LineSegment(self.startPoint, Point(0,0))
        piece2 = LineSegment(Point(0,0), self.endPoint)

        normalLine = circle.originPoint.shortestLineSegment(self)

        juncX = normalLine.endPoint.x + normalLine.dirVector[0]
        juncY = normalLine.endPoint.y + normalLine.dirVector[1]

        junc = Point(juncX, juncY)
        piece1.endPoint = junc
        piece2.startPoint = junc

        print("Alt path: ") 
        piece1.print()
        piece2.print()
        print("\n")
        return piece1, piece2

class Circle:
    def __init__(self, originPoint, radius):
        self.originPoint = originPoint
        self.radius = radius

# Test case 1
l1 = LineSegment(Point(0,10), Point(30,10))
c1 = Circle(Point(12,0), 10)

l1.intersectsCircle(c1)

# Test case 2
l2 = LineSegment(Point(0,-10), Point(15,15))
c2 = Circle(Point(9,3), 5)

l2.intersectsCircle(c2)

# Test case 3
l3 = LineSegment(Point(0,-10), Point(15,15))
c3 = Circle(Point(10,-5), 4)

l3.intersectsCircle(c3)



